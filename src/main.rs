fn main() {
    println!(
        "{}",
        smallest("hello", "world", "dog cat hello cat dog dog cat world")
    );
}

fn smallest(w1: &str, w2: &str, text: &str) -> i32 {
    let words: Vec<&str> = text.split(" ").collect();
    dbg!(words.clone());
    let mut l1: Vec<i32> = vec![];
    let mut l2: Vec<i32> = vec![];
    for i in 0..words.len() {
        if words[i] == w1 {
            l1.push(i as i32);
        } else if words[i] == w2 {
            l2.push(i as i32);
        }
    }
    dbg!(l1.clone());
    dbg!(l2.clone());
    let mut min = i32::MAX;
    for i in l1 {
        for j in l2.clone() {
            let tmp = i - j;
            if tmp.abs() < min {
                min = tmp.abs();
            }
        }
    }
    min - 1
}
